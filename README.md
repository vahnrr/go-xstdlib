> :warning: **PLEASE DON'T USE THIS LIBRARY IN A PRODUCTION ENVIRONMENT.**
>
> It's a toy project that offers no guarantees of public API stability.
> If I realise I made a messed up design decision I might change the public API.
> Instead please copy the bits you like and give feedback on things to improve, cheers!

# go-xstdlib

> Experimental extensions to the Go standard library.

- All functions have an example usage and a test suite
- Test coverage is ensured using Go's internal tooling (see `make` output)
