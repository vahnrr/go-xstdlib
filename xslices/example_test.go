package xslices_test

import (
	"fmt"
	"os"
	"regexp"

	"gitlab.com/vahnrr/go-xstdlib/xslices"
)

func ExampleCloneFunc() {
	input := []string{"hel|o", "mat[ch)es"}
	escapedInput := xslices.CloneFunc(input, regexp.QuoteMeta)
	fmt.Println(escapedInput)
	// Output:
	// [hel\|o mat\[ch\)es]
}

func ExampleGet() {
	inputSlice := []string{"zero", "one", "two"}
	one, ok := xslices.Get(inputSlice, 1)
	if !ok {
		fmt.Fprintln(os.Stderr, "no entry at index 1 of slice")
	}
	fmt.Println(one)
	// Output:
	// one
}

func ExampleLast() {
	inputSlice := []int{0, 1, 2, 3}
	last, ok := xslices.Last(inputSlice)
	if !ok {
		fmt.Fprintln(os.Stderr, "the slice is empty")
	}
	fmt.Println(last)
	// Output:
	// 3
}
