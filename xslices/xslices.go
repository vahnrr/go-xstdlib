// Package xslices provides extensions to the standard library's [slices] package.
package xslices

import "slices"

// CloneFunc is like [slices.Clone] but it also processes the slice's elements through a function.
// The elements are copied using assignment, so this is a shallow clone.
func CloneFunc[S ~[]E, E any](s S, fn func(E) E) S {
	clone := slices.Clone(s)
	if fn == nil {
		return clone
	}

	for i, e := range clone {
		clone[i] = fn(e)
	}
	return clone
}

// Get returns the i element of the slice and whether or not it exists.
// Similar to accessing a map's value with a key.
func Get[E any](s []E, i int) (E, bool) {
	if (i >= 0) && (i < len(s)) {
		return s[i], true
	}

	// Return the type's default value.
	return *new(E), false
}

// Last returns the last element of the slice and whether or not it exists.
// Similar to accessing a map's value with a key.
// No last value would exist when an empty slice is given.
func Last[E any](s []E) (E, bool) {
	return Get(s, len(s)-1)
}
