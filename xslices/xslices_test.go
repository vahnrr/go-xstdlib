package xslices_test

import (
	"reflect"
	"regexp"
	"strings"
	"testing"

	. "gitlab.com/vahnrr/go-xstdlib/xslices"
)

type dummy struct {
	Value uint
}

func TestCloneFunc(t *testing.T) {
	type TestCase[E any] struct {
		input []E
		fn    func(E) E
		want  []E
	}

	intTestCases := map[string]TestCase[int]{
		"increment all integers of a slice by 2": {
			input: []int{-2, 0, 4},
			fn:    func(i int) int { return i + 2 },
			want:  []int{0, 2, 6},
		},
		"divide all integers of a slice by 3": {
			input: []int{3, 12, 33},
			fn:    func(i int) int { return i / 3 },
			want:  []int{1, 4, 11},
		},
		"nil function returns a shallow clone of the input": {
			input: []int{0, 3, 6, 9},
			fn:    nil,
			want:  []int{0, 3, 6, 9},
		},
		"nil slice through divide by 3 function returns a nil slice": {
			input: nil,
			fn:    func(i int) int { return i / 3 },
			want:  nil,
		},
		"nil slice through nil function returns nil slice": {
			input: nil,
			fn:    nil,
			want:  nil,
		},
	}

	stringTestCases := map[string]TestCase[string]{
		"escape all special regexp characters": {
			input: []string{"hel|o", "mat[ch)es"},
			fn:    regexp.QuoteMeta,
			want:  []string{`hel\|o`, `mat\[ch\)es`},
		},
		"trim spaces": {
			input: []string{"	word ", "   when	"},
			fn:    strings.TrimSpace,
			want:  []string{"word", "when"},
		},
		"append .bak": {
			input: []string{"file.txt", "save.bin"},
			fn:    func(e string) string { return e + ".bak" },
			want:  []string{"file.txt.bak", "save.bin.bak"},
		},
	}

	type subSt struct {
		b bool
	}
	type st struct {
		i   int
		s   string
		sub subSt
	}
	structTestCases := map[string]TestCase[st]{
		"slices of structures can be cloned": {
			input: []st{
				st{i: 0, s: "ex", sub: subSt{b: true}},
				st{i: 4, s: "op", sub: subSt{b: false}},
			},
			fn: func(in st) st {
				in.i += 2
				in.s += "e"
				in.sub.b = false
				return in
			},
			want: []st{
				st{i: 2, s: "exe", sub: subSt{b: false}},
				st{i: 6, s: "ope", sub: subSt{b: false}},
			},
		},
	}

	// Ensure that:
	// - The output is always a shallow clone of the input (i.e. don't modify the input slice)
	// - The output's entries have the expected values

	for description, tc := range intTestCases {
		t.Run(description, func(t *testing.T) {
			got := CloneFunc(tc.input, tc.fn)
			if (&got == &tc.input) || (!reflect.DeepEqual(got, tc.want)) {
				t.Fatalf("CloneFunc(%v, _) = %v, want: %v", tc.input, got, tc.want)
			}
		})
	}

	for description, tc := range stringTestCases {
		t.Run(description, func(t *testing.T) {
			got := CloneFunc(tc.input, tc.fn)
			if (&got == &tc.input) || (!reflect.DeepEqual(got, tc.want)) {
				t.Fatalf("CloneFunc(%v, _) = %v, want: %v", tc.input, got, tc.want)
			}
		})
	}

	for description, tc := range structTestCases {
		t.Run(description, func(t *testing.T) {
			got := CloneFunc(tc.input, tc.fn)
			if (&got == &tc.input) || (!reflect.DeepEqual(got, tc.want)) {
				t.Fatalf("CloneFunc(%v, _) = %v, want: %v", tc.input, got, tc.want)
			}
		})
	}
}

func TestGet(t *testing.T) {
	type TestCase[E any] struct {
		inputSlice []E
		inputIndex int
		wantValue  E
		wantOk     bool
	}

	intTestCases := map[string]TestCase[int]{
		"int: index exists in slice": {
			inputSlice: []int{9, 8, 7},
			inputIndex: 2,
			wantValue:  7,
			wantOk:     true,
		},
		"int: index out of slice bounds": {
			inputSlice: []int{-5},
			inputIndex: 3,
			wantValue:  0,
			wantOk:     false,
		},
		"int: empty slice": {
			inputSlice: []int{},
			inputIndex: 1,
			wantValue:  0,
			wantOk:     false,
		},
		"int: uninitialised slice": {
			inputSlice: []int(nil),
			inputIndex: 1,
			wantValue:  0,
			wantOk:     false,
		},
	}

	stringTestCases := map[string]TestCase[string]{
		"string: index exists in slice": {
			inputSlice: []string{"one", "two"},
			inputIndex: 0,
			wantValue:  "one",
			wantOk:     true,
		},
		"string: index out of slice bounds": {
			inputSlice: []string{"alone"},
			inputIndex: 3,
			wantValue:  "",
			wantOk:     false,
		},
		"string: empty slice": {
			inputSlice: []string{},
			inputIndex: 1,
			wantValue:  "",
			wantOk:     false,
		},
		"string: uninitialised slice": {
			inputSlice: []string(nil),
			inputIndex: 1,
			wantValue:  "",
			wantOk:     false,
		},
	}

	dummyTestCases := map[string]TestCase[dummy]{
		"dummy: index exists in slice": {
			inputSlice: []dummy{{Value: 8}, {Value: 2}},
			inputIndex: 0,
			wantValue:  dummy{Value: 8},
			wantOk:     true,
		},
		"dummy: index out of slice bounds": {
			inputSlice: []dummy{{Value: 42}},
			inputIndex: 3,
			wantValue:  dummy{},
			wantOk:     false,
		},
		"dummy: empty slice": {
			inputSlice: []dummy{},
			inputIndex: 1,
			wantValue:  dummy{},
			wantOk:     false,
		},
		"dummy: uninitialised slice": {
			inputSlice: []dummy(nil),
			inputIndex: 1,
			wantValue:  dummy{},
			wantOk:     false,
		},
	}

	for description, tc := range intTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Get(tc.inputSlice, tc.inputIndex)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v, %v) = %v, %v; want: %v, %v",
					tc.inputSlice, tc.inputIndex, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}

	for description, tc := range stringTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Get(tc.inputSlice, tc.inputIndex)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v, %v) = %v, %v; want: %v, %v",
					tc.inputSlice, tc.inputIndex, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}

	for description, tc := range dummyTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Get(tc.inputSlice, tc.inputIndex)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v, %v) = %v, %v; want: %v, %v",
					tc.inputSlice, tc.inputIndex, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}
}

func TestLast(t *testing.T) {
	type TestCase[E any] struct {
		input     []E
		wantValue E
		wantOk    bool
	}

	intTestCases := map[string]TestCase[int]{
		"int: filled slice": {
			input:     []int{9, 8, 7},
			wantValue: 7,
			wantOk:    true,
		},
		"int: single entry slice": {
			input:     []int{-5},
			wantValue: -5,
			wantOk:    true,
		},
		"int: empty slice": {
			input:     []int{},
			wantValue: 0,
			wantOk:    false,
		},
		"int: uninitialised slice": {
			input:     []int(nil),
			wantValue: 0,
			wantOk:    false,
		},
	}

	stringTestCases := map[string]TestCase[string]{
		"string: filled slice": {
			input:     []string{"one", "two"},
			wantValue: "two",
			wantOk:    true,
		},
		"int: single entry slice": {
			input:     []string{"alone"},
			wantValue: "alone",
			wantOk:    true,
		},
		"int: empty slice": {
			input:     []string{},
			wantValue: "",
			wantOk:    false,
		},
		"int: uninitialised slice": {
			input:     []string(nil),
			wantValue: "",
			wantOk:    false,
		},
	}

	dummyTestCases := map[string]TestCase[dummy]{
		"dummy: filled slice": {
			input:     []dummy{{Value: 1}, {Value: 6}},
			wantValue: dummy{Value: 6},
			wantOk:    true,
		},
		"dummy: single entry slice": {
			input:     []dummy{{Value: 5}},
			wantValue: dummy{Value: 5},
			wantOk:    true,
		},
		"dummy: empty slice": {
			input:     []dummy{},
			wantValue: dummy{},
			wantOk:    false,
		},
		"dummy: uninitialised slice": {
			input:     []dummy(nil),
			wantValue: dummy{},
			wantOk:    false,
		},
	}

	for description, tc := range intTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Last(tc.input)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v) = %v, %v; want: %v, %v",
					tc.input, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}

	for description, tc := range stringTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Last(tc.input)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v) = %v, %v; want: %v, %v",
					tc.input, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}

	for description, tc := range dummyTestCases {
		t.Run(description, func(t *testing.T) {
			gotValue, gotOk := Last(tc.input)
			if (gotValue != tc.wantValue) || (gotOk != tc.wantOk) {
				t.Fatalf("Get(%v) = %v, %v; want: %v, %v",
					tc.input, gotValue, gotOk, tc.wantValue, tc.wantOk)
			}
		})
	}
}
