package xmath_test

import (
	"fmt"

	"gitlab.com/vahnrr/go-xstdlib/xmath"
)

func ExampleAbsInt() {
	fmt.Println(xmath.AbsInt(-3))
	fmt.Println(xmath.AbsInt(55))
	// Output:
	// 3
	// 55
}
