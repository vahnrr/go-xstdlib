package xmath_test

import (
	"math"
	"testing"

	. "gitlab.com/vahnrr/go-xstdlib/xmath"
)

func TestAbsInt(t *testing.T) {
	testCases := map[string]struct {
		input int
		want  int
	}{
		"negative becomes positive": {
			input: -4,
			want:  4,
		},
		"positive stays positive": {
			input: 5,
			want:  5,
		},
		"zero stays zero": {
			input: 0,
			want:  0,
		},
		"NaN stays NaN": {
			input: int(math.NaN()),
			want:  math.MaxInt,
		},
		"-Inf becomes math.MaxInt": {
			input: int(math.Inf(-1)),
			want:  math.MaxInt,
		},
		"+Inf becomes math.MaxInt": {
			input: int(math.Inf(1)),
			want:  math.MaxInt,
		},
		"math.MinInt becomes math.MaxInt": {
			input: math.MinInt,
			want:  math.MaxInt,
		},
		"math.MaxInt stays max.MaxInt": {
			input: math.MaxInt,
			want:  math.MaxInt,
		},
	}

	for description, tc := range testCases {
		t.Run(description, func(t *testing.T) {
			got := AbsInt(tc.input)
			if got != tc.want {
				t.Fatalf("AbsInt(%v) = %v, want: %v", tc.input, got, tc.want)
			}
		})
	}
}
