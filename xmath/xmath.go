// Package xmath provides extensions to the standard library's [math] package.
package xmath

import "math"

// AbsInt returns the absolute value of x.
//
// Special cases are:
//
//	Abs(±Inf) = math.MaxInt
//	Abs(NaN) = math.MaxInt
func AbsInt(x int) int {
	// Just like [math.Abs] special cases need to be handled.
	if (x <= math.MinInt) || (x >= math.MaxInt) {
		return math.MaxInt
	}

	// When benchmarked against the bitwise version, the speed results were very similar.
	// Since the condition below feels simpler to read, it's more inline with Go's philosophy.
	if x < 0 {
		return -x
	}
	return x
}
