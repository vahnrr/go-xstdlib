# Explicitly declare these as rules not files to draw the dependency tree with.
.PHONY: help test test-coverage test-coverage-profile test-verbose

COVERAGE_PROFILE := 'coverage.txt'

help: ## Show this help message.
	$(info # $@)
# Retrieve any documented task, alphabetically sort them then list them out, format:
# <task-name>: [<task-dependencies> ]## <task-documentation>
#
# NOTE: Field separator (-F) matches everything between the task's name to its documentation, so
# that tasks' that declare dependencies can still be documented.
	@awk -F ':.*? ## ' '/^[a-zA-Z0-9_-]+:.*? ## .*$$/ { printf "- %-20s %s\n", $$1, $$2 }' \
		$(MAKEFILE_LIST) | sort

# Catch unknown rules, warn then show the help.
%:
	$(info Unknown make rule: $@)
	@$(MAKE) -s help

test: ## Run the test suite.
	$(info # $@)
	go test ./...

test-coverage: ## Calculate the test coverage percentage.
	$(info # $@)
	go test -cover ./...

test-coverage-report: ## Generate and open the HTML coverage profile report.
	$(info # $@)
	go test -coverprofile=$(COVERAGE_PROFILE) ./...
	go tool cover -html=$(COVERAGE_PROFILE)
	rm $(COVERAGE_PROFILE)

test-verbose: ## Run the test suite (verbosely).
	$(info # $@)
	go test -v ./...
