package channels_test

import (
	"fmt"
	"testing"

	"gitlab.com/vahnrr/go-xstdlib/channels"
)

func TestFanIn(t *testing.T) {
	testCases := map[string]struct {
		input string // Letters to send.
		want  int    // Number of letters received.
	}{
		"alphabet": {
			input: "abcdefghijklmnopqrstuvwxyz",
			want:  52,
		},
		"four": {
			input: "four",
			want:  8,
		},
		"nothing": {
			input: "",
			want:  0,
		},
	}

	process := func(in <-chan rune, out chan rune) {
		for r := range in {
			out <- r
		}
	}

	for description, tc := range testCases {
		t.Run(description, func(t *testing.T) {
			in1 := make(chan rune)
			in2 := make(chan rune)

			go func() {
				for _, r := range tc.input {
					in1 <- r
					in2 <- r
				}

				close(in1)
				close(in2)
			}()

			gotLetters := make([]rune, 0)
			for r := range channels.FanIn(process, in1, in2) {
				gotLetters = append(gotLetters, r)
			}
			got := len(gotLetters)

			if got != tc.want {
				t.Fatalf("want %d, got %d", tc.want, got)
			}
		})
	}

	t.Run("no input channels", func(t *testing.T) {
		inputs := make([]<-chan rune, 0)
		got := make([]rune, 0)
		for r := range channels.FanIn(process, inputs...) {
			got = append(got, r)
		}
		if len(got) > 0 {
			t.Fatalf("want 0, got %d", got)
		}
	})
}

func ExampleFanIn() {
	// Double every input's value and send it to the output.
	process := func(in <-chan int, out chan int) { // [channels.FanInProcessFunc[int]]
		for n := range in {
			out <- n * 2
		}
	}

	// Initialise the input channels.
	in1 := make(chan int)
	in2 := make(chan int)
	in3 := make(chan int)

	go func() {
		// Send data to all input channels.
		for i := 1; i < 5; i++ {
			in1 <- i
			in2 <- i * 2
			in3 <- i * 3
		}

		// Close the input channels once everything was sent to them.
		close(in1)
		close(in2)
		close(in3)
	}()

	// Iterate over values received from the output channel.
	sum := 0
	for n := range channels.FanIn(process, in1, in2, in3) {
		sum += n
	}

	fmt.Println(sum)
	// Output:
	// 120
}
