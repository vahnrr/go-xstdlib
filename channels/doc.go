/*
Package channels implements generic channel patterns.

Channel types reminder:

- [<-chan int] is a receive-only channel, data can only be received from it, it would produce an
error/panic if data was sent into it

- [chan<- int] is a send-only channel, data can only be sent into it, it would produce an
error/panic if any channel/variable were to receive data from it

- [chan int] is a bi-directional channel

Closing a channel using the [close] builtin means it can't receive data anymore, however it can
still be read from.
*/
package channels
