package channels

import "sync"

// FanInProcessFunc represents an operation that receives values from the in(put) channel, processes
// them to then send the processing result to the out(put) channel.
type FanInProcessFunc[T any] func(in <-chan T, out chan T)

// FanIn receives from multiple input channels and proceeds until all of them are closed by
// multiplexing them into a single out(put) channel.
func FanIn[T any](process FanInProcessFunc[T], inputs ...<-chan T) <-chan T {
	out := make(chan T)
	// Exit early when an empty input was given.
	if len(inputs) == 0 {
		close(out)
		return out
	}

	// Register all input channels into the [sync.WaitGroup] so that the function can wait until all
	// of the inputs' data is received and processed before returning.
	var wg sync.WaitGroup
	wg.Add(len(inputs))

	// Start a processing goroutine for each input channel that writes to the common output channel.
	for _, in := range inputs {
		go func() {
			process(in, out)
			wg.Done()
		}()
	}

	// Once all processings are done and the input channels are closed, the output channel can be
	// closed as well.
	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}
