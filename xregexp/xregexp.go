// Package xregexp provides extensions to the standard library's [regexp] package.
package xregexp

import "regexp"

// Regexp is the representation of a compiled regular expression.
//
// This library's [Regexp] embeds the original [regexp.Regexp] from the standard library.
type Regexp struct {
	*regexp.Regexp
}

// FindStringNamedSubmatch returns a map of named capture groups to their match of the regular
// expression in s, if any, of its subexpressions, as defined by the 'Submatch' description in the
// [regexp.Regexp] package comment.
// A return value of an empty map indicates no match.
func (re *Regexp) FindStringNamedSubmatch(s string) map[string]string {
	names := re.SubexpNames()
	if (len(names) == 1) && (names[0] == "") {
		return make(map[string]string)
	}

	matches := re.FindStringSubmatch(s)
	if len(matches) == 0 {
		return make(map[string]string)
	}

	namedMatches := make(map[string]string, len(names))
	for i, v := range matches {
		// When unnamed capture groups match, their map key would be "".
		// However only named capture groups should be part of the returned map.
		if names[i] != "" {
			namedMatches[names[i]] = v
		}
	}

	return namedMatches
}
