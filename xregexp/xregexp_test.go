package xregexp_test

import (
	"reflect"
	"regexp"
	"testing"

	. "gitlab.com/vahnrr/go-xstdlib/xregexp"
)

func TestRegexp_FindStringNamedSubmatch(t *testing.T) {
	testCases := map[string]struct {
		exp   string
		input string
		want  map[string]string
	}{
		"no submatches at all": {
			exp:   `(\w+)`,
			input: "---",
			want:  map[string]string{},
		},
		"single unnamed submatch": {
			exp:   `(\w+)`,
			input: "original_content",
			want:  map[string]string{},
		},
		"single named submatch": {
			exp:   `(?P<single>\w+)`,
			input: "original_content",
			want: map[string]string{
				"single": "original_content",
			},
		},
		"single named submatch that does not get matched": {
			exp:   `(?P<single>\w+)`,
			input: "-#-",
			want:  map[string]string{},
		},
		"multiple unnamed submatches": {
			exp:   `(\w+) - ([0-9]+) - ([#@_]+)`,
			input: "hi_mate - 1289 - _@_#_",
			want:  map[string]string{},
		},
		"multiple named submatches": {
			exp:   `(?P<1st>\w+) - (?P<2nd>[0-9]+) - (?P<3rd>[#@_]+)`,
			input: "hi_mate - 1289 - _@_#_",
			want: map[string]string{
				"1st": "hi_mate",
				"2nd": "1289",
				"3rd": "_@_#_",
			},
		},
		"multiple submatches with some unnamed": {
			exp:   `(?P<start>\w+)/([0-9]+)/(?P<end>\w+)/([&_]+)`,
			input: "meeting/007/the_agent/_&_",
			want: map[string]string{
				"start": "meeting",
				"end":   "the_agent",
			},
		},
		"multiple named submatches with some that do not get matched": {
			exp:   `(?P<1st>\w+) - (?P<2nd>[0-9]+) - (?P<3rd>[#@_]+)`,
			input: "hi_mate - not_matched - _@_#_",
			want:  map[string]string{},
		},
		"no named submatches": {
			exp:   `\w+ - \w+`,
			input: "hello - world",
			want:  map[string]string{},
		},
	}

	for description, tc := range testCases {
		t.Run(description, func(t *testing.T) {
			re, err := regexp.Compile(tc.exp)
			if err != nil {
				t.Fatalf("cannot compile expression: %s", err)
			}
			xre := &Regexp{re}

			got := xre.FindStringNamedSubmatch(tc.input)
			if !reflect.DeepEqual(got, tc.want) {
				t.Fatalf("%q.FindStringNamedSubmatch(%v) = %v, want: %v",
					xre.String(), tc.input, got, tc.want)
			}
		})
	}
}
