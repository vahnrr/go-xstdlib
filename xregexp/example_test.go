package xregexp_test

import (
	"fmt"
	"regexp"

	"gitlab.com/vahnrr/go-xstdlib/xregexp"
)

func ExampleRegexp_FindStringNamedSubmatch() {
	re := regexp.MustCompile(`(?P<1st>\w+) - (?P<2nd>[0-9]+) - (?P<3rd>[#@_]+)`)
	xre := &xregexp.Regexp{re}
	fmt.Println(xre.FindStringNamedSubmatch("hi_mate - 1289 - _@_#_"))
	// Output:
	// map[1st:hi_mate 2nd:1289 3rd:_@_#_]
}
